package com.magasin.api_magasin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public final class ApiMagasinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMagasinApplication.class, args);
	}

}
