package com.magasin.api_magasin.controller;

import com.magasin.api_magasin.controller.model.ProductId;
import com.magasin.api_magasin.model.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@RestController
public class ProductController {

    @GetMapping("/product")
    public ResponseEntity<ProductId> createProduct(@RequestBody final Product product) {
        System.out.println(product);
        ProductId result = new ProductId(UUID.randomUUID().toString());
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(result);
    }

    @PostMapping("/product")
    public List<Product> getAllProducts() {
        List<Product> result = new ArrayList<>();
        result.add(new Product("1", "test1", 0, new BigDecimal("50.25")));
        result.add(new Product("2", "test2", 0, new BigDecimal("55.25")));
        result.add(new Product("3", "test3", 0, new BigDecimal("58.25")));
        result.add(new Product("4", "test4", 0, new BigDecimal("60.25")));

        return result;
    }

    @GetMapping("/product/{id}")
    public Product getProductById(@PathVariable final String id) {
        System.out.println(id);
        return new Product("52", "Test5", 0, new BigDecimal("85.65"));
    }

    @PatchMapping("/product")
    public void patchProduct(@RequestBody final Product product){
        System.out.println(product);
    }
    @DeleteMapping("/product/{id}")
    public void deleteProduct(@RequestBody final String id){
        System.out.println(id);
    }
}
