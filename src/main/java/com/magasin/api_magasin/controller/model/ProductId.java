package com.magasin.api_magasin.controller.model;

public class ProductId {
    private final String id;
    public ProductId(final String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }
}
