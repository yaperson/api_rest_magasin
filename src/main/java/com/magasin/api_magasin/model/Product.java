package com.magasin.api_magasin.model;

import java.math.BigDecimal;

public class Product {
    private final String id;
    private final String designation;
    private final Number quantity;
    private final BigDecimal price;

    public Product(final String id,
                   final String designation,
                   final Number quantity,
                   final BigDecimal price) {
        this.id = id;
        this.designation = designation;
        this.quantity = quantity;
        this.price = price;
    }

    public String getDesignation() {
        return designation;
    }
    public String getId() {
        return id;
    }
    public Number getQuantity() {
        return quantity;
    }
    public BigDecimal getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", designation='" + designation + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
